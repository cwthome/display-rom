#!/bin/bash
# Set version 
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
pushd "$DIR" > /dev/null

export VNUM=${1}
export VERS=${1}-$(git rev-parse --short HEAD)
# echo $VERS

sed -r -i "s#ro.build.kernel.version=.*#ro.build.kernel.version=${VNUM}#" bootimg/ramdisk/default.prop
sed -r -i "s#ro.build.boot.version=.*#ro.build.boot.version=${VERS}#" bootimg/ramdisk/default.prop
dos2unix -q bootimg/ramdisk/default.prop 

perl -0777 -i -pe "s#navdy_hud_6dl:5.1.1/NAVDY_HUD/.*?:#navdy_hud_6dl:5.1.1/NAVDY_HUD/${VNUM}:#" bootimg/ramdisk/selinux_version
dos2unix -q bootimg/ramdisk/default.prop


perl -0777 -i -pe "s#navdy_hud_6dl:5.1.1/NAVDY_HUD/.*?:#navdy_hud_6dl:5.1.1/NAVDY_HUD/${VNUM}:#" bootimg/ramdisk/selinux_version
dos2unix -q bootimg/ramdisk/default.prop


sed -r -i "s#ro.build.version.incremental=.*#ro.build.version.incremental=${VNUM}#" system/build.prop
perl -0777 -i -pe "s#ro.build.description=navdy_hud_6dl-user 5.1.1 NAVDY_HUD \S+ release-keys#ro.build.description=navdy_hud_6dl-user 5.1.1 NAVDY_HUD ${VERS} release-keys#" system/build.prop
perl -0777 -i -pe "s#ro.build.fingerprint=Navdy/navdy_hud_6dl/navdy_hud_6dl:5.1.1/NAVDY_HUD/.*?:#ro.build.fingerprint=Navdy/navdy_hud_6dl/navdy_hud_6dl:5.1.1/NAVDY_HUD/${VNUM}:#" system/build.prop
dos2unix -q system/build.prop
