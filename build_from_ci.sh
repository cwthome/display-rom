#!/bin/bash
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
pushd "$DIR" &> /dev/null

set -e # Any commands which fail will cause the shell script to exit

#apt install -y dos2unix sudo gawk android-tools-fsutils zip coreutils cpio

VNUM=$(git describe --tags | cut -d'-' -f1)
# If not on tag, increment previous one
ON_TAG=$(git describe --exact-match --tags HEAD 2> /dev/null)
test $ON_TAG || VNUM=$((VNUM + 1))
VERS=${VNUM}-$(git rev-parse --short HEAD)

echo -e "\n"\
"***********************\n"\
" Building ${VERS}     \n"\
"***********************\n"

bash ./bootimg/unpackimg.sh boot.img

bash ./set_version.sh $VNUM

echo '
****************
 Build boot.img
****************'
bash ./bootimg/repackimg.sh
mv ./bootimg/image-new.img ./boot.img
sudo chown $(whoami) boot.img


build_system_dist () {

echo '
******************
 Build system.img
******************'
./build_system_img.sh

OUT=$DIR/dist/navdy-display-alelec-${VERS}$1
rm -rf ${OUT} 2> /dev/null
rm -rf ${OUT}.zip 2> /dev/null
mkdir -p ${OUT}


echo '
***************
 Assemble Dist
***************'
cp -l boot.img $OUT/
mv system_new.img $OUT/system.img
cp -l recovery.img $OUT/
cp -l FLASH* $OUT/
cp -l FACTORY_RESET* $OUT/
cp -l WIN_INSTALL_USB_DRIVER.bat $OUT/
cp -l android-info.txt $OUT/

pushd $OUT &> /dev/null
for IMG in system.img boot.img recovery.img; do
echo "$IMG $(stat -c %s $IMG) $(sha256sum $IMG | cut -d' ' -f1)" >> info.txt
done
echo "version: ${VNUM}" >> info.txt
popd &> /dev/null

mkdir $OUT/tools
cp -al tools/usb_driver_windows $OUT/tools/
cp -al tools/nexus-tools-673c086 $OUT/tools/
cp -al tools/linux $OUT/tools/
cp -al tools/windows $OUT/tools/
cp -al tools/macos $OUT/tools/

echo $OUT
ls -l $OUT
pushd $OUT
zip -r $OUT.zip *
popd
echo "
Built ${OUT}
"
}


BRANCH=$(test $ON_TAG && echo $ON_TAG || echo "master")

echo '
******************
  RELEASE BUILD
******************'
perl -0777 -i -pe 's#ro\.build\.type=.*\n#ro.build.type=user\n#' system/build.prop
perl -0777 -i -pe 's#:userdebug/release-keys#:user/release-keys#' system/build.prop

rm -f ./Hud/*.apk ./Hud/*.zip
python3.6 ./download_apks.py --proj=Hud --branch=${BRANCH} --job=release
ls ./Hud/Hud-*${VNUM}*-release.apk
cp ./Hud/Hud-*${VNUM}*-release.apk ./system/priv-app/Hud/Hud.apk
unzip -o ./Hud/lib-armeabi-v7a.zip -d ./system/priv-app/Hud/lib/arm/

rm -f ./Obd/*.apk
python3.6 ./download_apks.py --proj=Obd --branch=${BRANCH} --job=release
ls ./Obd/Obd-*-release.apk
cp ./Obd/Obd-*-release.apk ./system/priv-app/Obd/Obd.apk

build_system_dist

echo '
******************
    DEBUG BUILD
******************'
perl -0777 -i -pe 's#ro\.build\.type=.*\n#ro.build.type=eng\n#' system/build.prop
perl -0777 -i -pe 's#:user/release-keys#:eng/release-keys#' system/build.prop

rm -f ./Hud/*.apk ./Hud/*.zip
python3.6 ./download_apks.py --proj=Hud --branch=${BRANCH} --job=debug
ls ./Hud/Hud-*${VNUM}*-debug.apk
cp ./Hud/Hud-*${VNUM}*-debug.apk ./system/priv-app/Hud/Hud.apk
unzip -o ./Hud/lib-armeabi-v7a.zip -d ./system/priv-app/Hud/lib/arm/

rm -f ./Obd/*.apk
python3.6 ./download_apks.py --proj=Obd --branch=${BRANCH} --job=debug
ls ./Obd/Obd-*-debug.apk
cp ./Obd/Obd-*-debug.apk ./system/priv-app/Obd/Obd.apk

build_system_dist -debug

echo '
DONE'
popd &> /dev/null
