import init.${ro.hardware}.usb.rc

on init
    #start watchdogd

    # See storage config details at http://source.android.com/tech/storage/
    mkdir /mnt/shell/emulated 0700 shell shell
    mkdir /storage/emulated 0555 root root

    export EXTERNAL_STORAGE /storage/emulated/legacy
    export EMULATED_STORAGE_SOURCE /mnt/shell/emulated
    export EMULATED_STORAGE_TARGET /storage/emulated

    # Support legacy paths
    symlink /storage/emulated/legacy /sdcard
    symlink /storage/emulated/legacy /mnt/sdcard
    symlink /storage/emulated/legacy /storage/sdcard0
    symlink /mnt/shell/emulated/0 /storage/emulated/legacy

on boot

    # Set permission for IIM node
    symlink /dev/mxs_viim /dev/mxc_mem

    #Disabled sleep mode for now
    write temporary > /sys/power/wake_unlock

    # Enable Tethering in the Settings
    setprop ro.tether.denied false

    # Set GPU 3D minimum clock to 8/64
    write /sys/bus/platform/drivers/galcore/gpu3DMinClock 8

    # 3D acceleration property
    setprop debug.sf.showfps    0
    setprop debug.sf.enable_hgl 1
    setprop debug.egl.hw	1

    setprop hwc.stretch.filter  1
    setprop hwc.enable_dither   1

    # fsl omx graphic manager media framework property
    #setprop media.omxgm.enable-player 1
    #setprop media.omxgm.enable-record 1
    #setprop media.omxgm.enable-scan 1
    #setprop rw.VIDEO_RENDER_NAME video_render.surface

    #Define the config for dual camera
    #setprop camera.disable_zsl_mode 1
    #For landscape mode, orient is 0
    #For portrait mode, orient is 90
    #the android before honycomb are all in portrait mode
    #setprop back_camera_name ov5640_mipi
    #setprop back_camera_orient 0
    #setprop front_camera_name uvc,ov5642_camera,ov5640_camera
    #setprop front_camera_orient 0
    setprop front_camera_name uvc
    setprop camera.disable_zsl_mode 1

    # Set OpenGLES version
    setprop ro.opengles.version 131072

    # Set rotation to 270 to cofigure as portrait mode
    setprop ro.sf.hwrotation 0

    # Set the density to 160dpi, default 128dpi is not good
    setprop ro.sf.lcd_density 160

    # Set extsd access permission
    setprop persist.sampling_profiler 1

    # emmulate battery property
    setprop sys.emulated.battery 1

    #GPS_RESET PIN
    write /sys/class/gpio/export 3

    write /sys/class/gpio/export 204
    write /sys/class/gpio/gpio204/direction "out"
    chmod 666 /sys/class/gpio/gpio204/value
    write /sys/class/gpio/gpio204/value 1

# change for FSL specific service
    chown root system /sys/devices/system/cpu/cpu0/cpufreq/scaling_max_freq
    chmod 0664 /sys/devices/system/cpu/cpu0/cpufreq/scaling_max_freq
    chown root system /sys/devices/system/cpu/cpu0/cpufreq/scaling_min_freq
    chmod 0664 /sys/devices/system/cpu/cpu0/cpufreq/scaling_min_freq
    chown root system /sys/devices/system/cpu/cpu0/cpufreq/scaling_governor
    chmod 0664 /sys/devices/system/cpu/cpu0/cpufreq/scaling_governor
    chown root system /sys/devices/system/cpu/cpu0/cpufreq/cpuinfo_cur_freq
    chmod 0440 /sys/devices/system/cpu/cpu0/cpufreq/cpuinfo_cur_freq
    chown root system /sys/devices/system/cpu/cpu0/cpufreq/scaling_available_frequencies
    chown root system /sys/devices/system/cpu/cpu0/online
    chmod 0664 /sys/devices/system/cpu/cpu0/online
    chown root system /sys/devices/system/cpu/cpu1/online
    chmod 0664 /sys/devices/system/cpu/cpu1/online
    chown root system /sys/devices/system/cpu/cpu2/online
    chmod 0664 /sys/devices/system/cpu/cpu2/online
    chown root system /sys/devices/system/cpu/cpu3/online
    chmod 0664 /sys/devices/system/cpu/cpu3/online

    mkdir /data/dalvik-cache
    chown system system /data/dalvik-cache

    # prepare for ril
    setprop gsm.ril.delay 15
    setprop ro.ril.wake_lock_timeout 300

    # No bluetooth hardware present
    #setprop wlan.interface wlan0
    setprop ro.bt.bdaddr_path "/sys/fsl_otp/HW_OCOTP_MAC0"
    chown bluetooth bluetooth /sys/fsl_otp/HW_OCOTP_MAC0
    chown bluetooth bluetooth /sys/fsl_otp/HW_OCOTP_MAC1
    chmod 0660 /sys/fsl_otp/HW_OCOTP_MAC0
    chmod 0660 /sys/fsl_otp/HW_OCOTP_MAC1

    chmod 665 /sys/class/rfkill/rfkill0/state
    chown bluetooth bluetooth /sys/class/rfkill/rfkill0/state
    write /sys/class/rfkill/rfkill0/state 0


    # dlpc
    chmod 666 /sys/dlpc/led_enable
    chmod 666 /sys/dlpc/RGB_Brightness
    chown system system /sys/dlpc/led_enable
    chown system system /sys/dlpc/RGB_Brightness
    #write /sys/dlpc/RGB_Brightness 1023:1023:1023:1:0
    restorecon_recursive /sys/dlpc

    # permissions for light sensor
    chmod 666 /sys/bus/i2c/drivers/tsl2x7x/2-0039/iio:device0/events/in_intensity0_thresh_period
    chmod 666 /sys/bus/i2c/drivers/tsl2x7x/2-0039/iio:device0/events/in_intensity0_thresh_falling_en
    chmod 666 /sys/bus/i2c/drivers/tsl2x7x/2-0039/iio:device0/events/in_intensity0_thresh_rising_en
    chmod 666 /sys/bus/i2c/drivers/tsl2x7x/2-0039/iio:device0/events/in_intensity0_thresh_falling_value
    chmod 666 /sys/bus/i2c/drivers/tsl2x7x/2-0039/iio:device0/events/in_intensity0_thresh_rising_value

    # Set WiFi Display Source VideoBitRate Adaptive
    setprop media.wfd.video-bitrate adaptive

    #KSM
    #write /sys/kernel/mm/ksm/pages_to_scan 100
    #write /sys/kernel/mm/ksm/sleep_millisecs 500
    #write /sys/kernel/mm/ksm/run 1

    start shutdownd

    start kmsglogger

    start ubootlogs

on property:ro.bootmode=quiet
    setprop sys.power.mode quiet

on property:ro.bootmode=normal
    setprop sys.power.mode normal
    start autobrightnessd

# mount the debugfs
    mount debugfs none /sys/kernel/debug/

# Set watchdog timer to 30 seconds and pet it every 10 seconds to get a 20 second margin
    service watchdogd /sbin/watchdogd 10 20
    class core
    seclabel u:r:watchdogd:s0

on charger
    # Enable Power modes and set the CPU Freq Sampling rates
    write /sys/devices/system/cpu/cpu0/cpufreq/scaling_governor "powersave"
    write /sys/devices/system/cpu/cpufreq/ondemand/up_threshold 90
    write /sys/devices/system/cpu/cpufreq/ondemand/sampling_rate 50000
    write /sys/devices/system/cpu/cpufreq/ondemand/io_is_busy 1
    write /sys/devices/system/cpu/cpufreq/ondemand/sampling_down_factor 4
    write /sys/devices/system/cpu/cpu1/online 0
    write /sys/devices/system/cpu/cpu2/online 0
    write /sys/devices/system/cpu/cpu3/online 0


service charger /charger
    class charger

# /data/media/0 maps to /sdcard
on property:dev.bootcomplete=1
    mkdir /data/media/0/.logs 775 media_rw media_rw
    start app_logs
    start iap_logs
    start obd_logs
    start lighting_logs
    start dial_logs

# save general logs
service app_logs /system/bin/logcat -v time -f /data/media/0/.logs/a.log -r 512 MFi:s ObdService:s ObdJob:s ObdCommand:s AutoConnect:s navdy_lights:s AutomaticBrightnessController:s DisplayPowerController:s NavdyLightSensor:s WebersLawBrightnessEstimator:s bt-navdy:s DialManager:s DialManagerScreen:s DialManagerView:s DialFirmwareUpdater:s
   class late_start
   disabled
   user media_rw

service iap_logs /system/bin/logcat -v time -f /data/media/0/.logs/mfi.log -r 256 *:s MFi
   class late_start
   disabled
   user media_rw

service obd_logs /system/bin/logcat -v time -f /data/media/0/.logs/obd.log -r 256 *:s ObdService ObdJob ObdCommand AutoConnect
   class late_start
   disabled
   user media_rw

service lighting_logs /system/bin/logcat -v time -f /data/media/0/.logs/lighting.log -n 8 -r 512 *:s navdy_lights AutomaticBrightnessController DisplayPowerController NavdyLightSensor WebersLawBrightnessEstimator
   class late_start
   disabled
   user media_rw

service dial_logs /system/bin/logcat -v time -f /data/media/0/.logs/dial.log -r 256 *:s bt-navdy DialManager DialManagerScreen DialManagerView DialFirmwareUpdater
   class late_start
   disabled
   user media_rw

# The following two services are managed by DummyNetNetworkFactory

# redsocks
service redsocks /system/xbin/redsocks2 -c /etc/redsocks.android.conf
    disabled

# ttdnsd
service ttdnsd /system/xbin/ttdnsd -d -c -u -f /etc/ttdnsd.conf
    disabled

# Temperature and Fan Control
service tempd /system/bin/tempd 15
    class late_start
    socket tempstatus    stream 666 system system

on property:config.mfg_image=1
    stop tempd
    write /sys/class/leds/fan-control/brightness 255

# Shutdown daemon
service shutdownd /system/bin/shutdownd
    socket shutdownd    stream 666 system system

# Kernel message logging daemon   we use "oneshot" because it forks
# a daemon process and exits so that init does not track it which
# allows it to keep logging during system shutdown
service kmsglogger /system/bin/kmsglogger
    oneshot
    user root
    group root

# Uboot message logging we use "oneshot" because it extracts
# the logs and exits
service ubootlogs  /system/bin/ubootlogs  /cache/ubootlogs.txt
    oneshot
    user root
    group root

# Monitor power
service powermon /system/bin/sh /system/etc/powermon.sh
    console
    user root
    group root
    oneshot
    disabled

# swiped
service swiped /system/bin/logwrapper /system/xbin/swiped
   class late_start
   socket swiped    dgram 666 system system u:r:swiped:s0
   disabled

# service names must be <= 16 characters long
service swiped-beta /system/bin/logwrapper /system/xbin/swiped-experimental
   class late_start
   socket swiped    dgram 666 system system u:r:swiped:s0
   disabled

on property:gesture.enabled=1
   stop swiped-beta
   start swiped

on property:gesture.enabled=beta
   stop swiped
   start swiped-beta

on property:gesture.enabled=0
   stop swiped
   stop swiped-beta

service autobrightnessd /system/bin/autobrightnessd
   oneshot

on property:hw.navdy.autobrightnessd=0
   stop autobrightnessd

on property:hw.navdy.autobrightnessd=1
   start autobrightnessd
